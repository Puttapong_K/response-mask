### How to use Response-Mask Library for akka framework ###

After you adding response-mask lib into your library dependency, you can use it by extend Response

For example,

Firstly, mock up example data
```
import core.Response

1. set up your controller to extend Response
class Controller extends Response {

case class data(id: Int, name: String)

val ok: Int = 200
val notFound: Int = 404
val message: String = "Not found any data"
val data: Data = Data(1,"example data")

*Hint you can call Errors by import core.ResponseFunction
val errors: Errors = Errors(notFound, message, "any type")

```
Secondly, Convert the data into response format by using two way
- response format (Json String)
- case class response format

For response format(Json String)
```
val successJsonString = buildSuccessFormatJsString(ok,data)
val failureJsonString = buildFailureFormatJsString(notFound,errors)

Response by Play framework
- By Using Results.Status (type1)
 val result = Status(ok)(successJsonString)

- By Using Results.Status (type2)
 val result = Ok(successJsonString)
```

 Or covert to case class response format (for dynamic your actions)
```
 val successFormat: SuccessFormat[Data] = toSuccessFormat[Data](ok,data)
 val failureFormat: FailureFormat[Error] = toFailureFormat(notFound,errors)
 
 for now you can covert response case class by using your own json library such as 
 play-json, or using Response
 
 - By covert by Response
    val jsonString =  successFormat.toJSFormatString
    val result = Status(notFound)(jsonString)
 
 - By covert by play-json
    val json = Json.toJson(successFormat)
    * Warning do not forget to adding the format of response case class 
 
```