### How to use Response-Mask Library for akka framework ###

After you adding response-mask lib into your library dependency, you can use it by extend Response

For example,

Firstly, mock up example data
```

import core.Response

1. set up your controller to extend Response
class Controller extends Response {

case class data(id: Int, name: String)

val ok: Int = 200
val notFound: Int = 404
val message: String = "Not found any data"
val data: Data = Data(1,"example data")

*Hint you can call Errors by import core.ResponseFunction
val errors: Errors = Errors(notFound, message, "any type")

```
Secondly, Convert the data into response format by using two way
- response format (Json String)
- case class response format

For response format(Json String)
```
val successJsonString = buildSuccessFormatJsString(ok,data)
val failureJsonString = buildFailureFormatJsString(notFound,errors)
```
Or covert to case class response format (for dynamic your actions)
```
 val successFormat: SuccessFormat[Data] = toSuccessFormat[Data](ok,data)
 val failureFormat: FailureFormat[Error] = toFailureFormat(notFound,errors)
```

Implement the responseFormat into your step buildResponse for example
```
  def successResponse[A](data: SuccessFormat[A], status_code: Int) =
    HttpResponse(status_code, entity = HttpEntity(ContentTypes.`application/json`, data.toJSFormatString))

  def successResponse(jsonStringFormat: String, status_code: Int) =
    HttpResponse(status_code, entity = HttpEntity(ContentTypes.`application/json`, jsonStringFormat))
```
When you want to use you can do it like below this.
```
val result = comeplete(successResponse(successJsonString, ok))
or
val result = comeplete(successResponse(successFormat.toJSFormatString,ok))

```