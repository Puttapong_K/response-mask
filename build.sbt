

name := "response-mask"

organization := "com.scale360"

version := "1.2"

scalaVersion := "2.12.1"

isSnapshot := true


publishTo := Some("Artifactory Realm" at "http://repo.dev.dotography.net/artifactory/sbt-lib-release")



credentials += Credentials("Artifactory Realm", "repo.dev.dotography.net", "deployer", "deployer")

val scalaTestVersion = "3.0.1"
val typesafeConfigVersion = "1.3.1"
val json4sConfigVersion = "3.5.3"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
  "com.typesafe" % "config" % typesafeConfigVersion,
  "org.json4s" %% "json4s-jackson" % json4sConfigVersion
)
