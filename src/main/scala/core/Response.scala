package core



import org.json4s.{Reader, _}

trait Response {

  import models.{Errors, FailureFormat, SuccessFormat}

  implicit val defaultFormat = DefaultFormats

  type JsonString = String

  def buildSuccessFormatJValue[A](code: Int, data: A): JValue = buildSuccessFormat(code, data).toJValue

  def buildSuccessFormatJsString[A](code: Int, data: A): JsonString = buildSuccessFormat(code, data).toJSFormatString

  def buildSuccessFormat[A](code: Int, data: A): SuccessFormat[A] = SuccessFormat(code, data)

  def buildFailureFormatJValue[B](code: Int, error: Errors[B]): JValue = buildFailureFormat(code, error).toJValue

  def buildFailureFormatJsString[B](code: Int, error: Errors[B]): JsonString = buildFailureFormat(code, error).toJSFormatString

  def buildFailureFormat[B](code: Int, error: Errors[B]): FailureFormat[B] = FailureFormat[B](code, error)

  def buildResponse[T](status_code: Int, data: T): JsonString = data match {
    case failure: Errors[T] => buildFailureFormatJsString[T](status_code, failure)
    case success => buildSuccessFormatJsString[T](status_code, success)
  }

}


class ResponseFunctional[T] {

  import org.json4s.jackson.Serialization.{read => readJs, writePretty => swrite}
  import scala.reflect.Manifest
  import org.json4s.jackson.JsonMethods

  type JsonString = Response#JsonString


  def readJSFromString(data: String)(implicit formats: Formats, mf: Manifest[T]): T = readJs[T](data)

  def fromJValue(jValue: JValue)(implicit format: Formats = DefaultFormats, mf: Manifest[T]): T = jValue.extract[T]

  def rename(name: String, newName: String)(implicit default: Formats = DefaultFormats, mf: Manifest[T]): Formats = default + FieldSerializer[T](FieldSerializer.renameTo(name, newName))

  def toJSFormatString(implicit format: Formats = DefaultFormats): JsonString = swrite(this)

  def parseJson(jsonInput: JsonInput): JValue = JsonMethods.parse(jsonInput)

  def parseJsonOpt(jsonInput: JsonInput): Option[JValue] = JsonMethods.parseOpt(jsonInput)


  def toJValue(implicit format: Formats = DefaultFormats): JValue = Extraction.decompose(this)

}


