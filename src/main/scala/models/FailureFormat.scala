package models

import core.ResponseFunctional


case class FailureFormat[B](code: Int, error: Errors[B]) extends ResponseFunctional[FailureFormat[B]]


case class Errors[B](code: Int, message: String, info: B)


object Errors {
    def apply[B](code: Int, message: Seq[String], info: B): Errors[B] = new Errors(code, message.mkString(","), info)
}