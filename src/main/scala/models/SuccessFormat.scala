package models

import core.ResponseFunctional


case class SuccessFormat[A](code: Int, data: A)  extends ResponseFunctional[SuccessFormat[A]]