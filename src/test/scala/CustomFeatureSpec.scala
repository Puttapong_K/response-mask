package scala

import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

abstract class CustomFeatureSpec extends FeatureSpec
  with GivenWhenThen
  with Matchers