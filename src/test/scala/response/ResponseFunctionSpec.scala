package response

import core.{Response, ResponseFunctional}
import org.json4s._


class ResponseFunctionSpec extends CustomFeatureSpec {

  case class BookId(value: String) {
    override def toString: String = "BookId(***)"
  }

  case class UserId(value: String) {
    override def toString: String = "UserId(***)"
  }


  case class User(userId: UserId, name: String, lastName: String, age: Int)

  case class Book(id: BookId, title: String, author: User) extends ResponseFunctional[Book]


  trait FeatureTest extends Response {
    val userId: UserId = UserId(value = "userId:1")
    val bookId: BookId = BookId(value = "bookId:1")
    val user: User = User(userId, "Ma", "Scale360", 24)
    val book: Book = Book(id = bookId, title = "How to use response covert", author = user)

  }

  new FeatureTest {

    feature("Rename field Json to new name") {
      scenario("I want to rename field title to book_name") {
        Given("A new name")
        val name = "title"
        val newName = "book_name"
        When("Call the rename")
        val format: Formats = book.rename(name, newName)
        val result: String = book.toJSFormatString(format)
        Then("The data of title should change to `book_name")
        val responseFunction = new ResponseFunctional()
        responseFunction.parseJson(result) \ s"$newName" should be(JString("How to use response covert"))
      }
    }

    feature("Read JValue") {
      scenario("I want to read JValue to case class") {
        Given("A JValue")
        val jValue = book.toJValue
        Then("The result should be the same as book")
        val responseFunctional = new ResponseFunctional[Book]
        responseFunctional.fromJValue(jValue).title should be(book.title)
      }
    }

    feature("Parse to Option Json") {
      scenario("I want to parse jsonInput to case class") {
        Given("A jsoninput")
        val jsonInput = book.toJSFormatString
        Then("The result should be the same as case class")
        val responseFunctional = new ResponseFunctional[Book]
        (responseFunctional.parseJsonOpt(jsonInput).get \ "title") should be(JString("How to use response covert"))
      }
    }
  }

}
