package response

import core.{Response, ResponseFunctional}
import models.{Errors, FailureFormat}
import org.json4s.JValue
import org.json4s.JsonAST._


class FailureResponseSpec extends CustomFeatureSpec {


  case class Info(field: String, error: String)

  trait FeatureTest extends Response {

    val info: Seq[Info] = Seq(Info(field = "free", error = "error database"), Info(field = "good", error = "Server Down"))

    val infoOpt: Seq[Option[String]] = Seq(Some("abc"), None, Some("error"))
    val notFound = 404
    val internalError = 500

    val errors: Errors[Seq[Info]] = Errors[Seq[Info]](code = internalError, message = "Internal Error", info)

    val errorsOpt: Errors[Seq[Option[String]]] = Errors(code = internalError, message = Seq("xxx"), infoOpt)

    val validFailureFormatOpt: FailureFormat[Seq[Option[String]]] = buildFailureFormat(internalError, error = errorsOpt)

    val validFailureFormat: FailureFormat[Seq[Info]] = buildFailureFormat(notFound, error = errors)

  }


  new FeatureTest {

    feature("Covert data to FailureFormat") {
      scenario("Covert a info into Failure Format") {
        When("Covert the data to dynamic response")
        val result: FailureFormat[Seq[Info]] = buildFailureFormat(notFound, error = errors)
        Then("The result should has type Failure format")
        result.isInstanceOf[FailureFormat[Seq[Info]]]
      }
    }

    feature("Convert failure format to type (Json String)") {
      scenario("I want to convert failure format to type (Json String)") {
        When("Covert to Json Format String")
        val result = validFailureFormat.toJSFormatString
        Then("The result should can read, and the data must be the same")
        val responseFunction = new ResponseFunctional[FailureFormat[Seq[Info]]]
        responseFunction.readJSFromString(result) == validFailureFormat
      }
    }

    feature("Convert option type in failure format to Json Format String") {
      scenario("I want to convert failure format to Json String") {
        When("Covert to Json Format String")
        val result = buildFailureFormatJsString(notFound, error = errorsOpt)
        Then("The result should can read, and the data must be the same")
        val responseFunction = new ResponseFunctional[FailureFormat[Seq[Option[String]]]]
        responseFunction.readJSFromString(result) == validFailureFormatOpt
      }
    }

    feature("Convert option type in failure format to Json Format String by BuildResponse") {
      scenario("I want to convert failure format to Json String") {
        When("Covert to Json Format String")
        val result = buildResponse(notFound, errorsOpt)
        Then("The result should can read, and the data must be the same")
        val responseFunction = new ResponseFunctional[FailureFormat[Seq[Option[String]]]]
        responseFunction.readJSFromString(result) == validFailureFormatOpt
      }
    }


    feature("Covert failure format to JValue Format") {
      scenario("I want to covert failure format to JObject") {
        Given("A expected result")
        val expected = JObject(List("code" -> JInt(notFound), "error" -> JObject(List("code" -> JInt(internalError), "message".->(JString("Internal Error")), "info" ->
          JArray(List(JObject(List("field" -> JString("free"), "error" -> JString("error database"))), JObject(List("field" -> JString("good"), "error" -> JString("Server Down")))))))))
        When("Convert failure format to JValue")
        val result: JValue = buildFailureFormatJValue(notFound, error = errors)
        Then("The result should be the same as JValue")
        result should be(expected)
      }
    }

  }
}
