package response

import core.{Response, ResponseFunctional}
import models.SuccessFormat
import org.json4s.JValue
import org.json4s.JsonAST._


class SuccessResponseSpec extends CustomFeatureSpec {

  case class Person(id: String, name: String, age: Int)


  trait FeatureTest extends Response {
    val person: Person = Person(id = "id:1", name = "Ma", age = 24)
    val ok: Int = 200
    val validSuccessFormat: SuccessFormat[Person] = buildSuccessFormat(ok, person)
  }

  new FeatureTest {
    feature("Convert data to SuccessFormat Response") {
      scenario("I want to covert a person case class to SuccessFormat Response") {
        When("Convert data to success response")
        val result: SuccessFormat[Person] = buildSuccessFormat(ok, person)
        Then("The result should be type SuccessFormat Response")
        result.isInstanceOf[SuccessFormat[Person]]
      }
    }

    feature("Convert SuccessFormat to type (Json String)") {
      scenario("I want to covert a Success format to Json String") {
        When("Covert success format to JsonString")
        val result: JsonString = validSuccessFormat.toJSFormatString
        Then("The result should can equal to Success Format")
        val responseFunction = new ResponseFunctional[SuccessFormat[Person]]
        responseFunction.readJSFromString(result) == validSuccessFormat
      }
    }

    feature("Convert data to Success Response type (Json Format)") {
      scenario("I want to covert a Success format to Json Format") {
        Given("A JObject expected")
        val expected = JObject(List("code" -> JInt(ok), "data" -> JObject(List("id" -> JString("id:1"), "name" -> JString("Ma"), "age" -> JInt(24)))))
        When("Covert to json successFormat")
        val result: JValue = buildSuccessFormatJValue(ok, person)
        println(Console.BLUE + result + Console.RESET)
        Then("The result should be the same as JValue")
        result should be(expected)
      }
    }

    feature("Convert the data to Success Response type (Json String)") {
      scenario("I want to covert the data to Success Response (Json String)") {
        When("Covert the data to Json String")
        val result: JsonString = buildResponse(ok, person)
        Then("The result should can equal to Success Format")
        val responseFunction = new ResponseFunctional[SuccessFormat[Person]]
        responseFunction.readJSFromString(result) == validSuccessFormat
      }
    }
  }
}
