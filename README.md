![Picture](https://media.licdn.com/media/AAEAAQAAAAAAAAhIAAAAJDZlOGFiNTE1LTM1OGItNGFhMy1iNGMyLTIwNGExNTMyZGUxZA.png)

# Response Mask #

This project was created for covert the generic case class data to
Response format. Read more about [Response format](https://bitbucket.org/dotography-code/scala-review-guidelines/src/bb0234a8f4c380bd67e3dfa58a75ca7dcb3807ee/guidelines/apis.md?at=master&fileviewer=file-view-default)

### MOTIVATIONS ###

Response format is needed, but each project have their own format. So I'm
make this project for the response format that can contain any data type class

### INSTALLING ###

* This project supports Scala version 2.12 only
* add the following dependency to your project
   
  `"com.scale360" %% "response-mask" % "1.2"`

### GETTING STARTED ###

* [See the example for Play framework](./docs/playframework.md)
* [See the example for akka framework](./docs/akkaframework.md)

#### Future Target ####

* Please request features by submitting issues with `feature request`


#### Maintainer ####

* Puttapong (Ma)
